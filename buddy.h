#ifndef BUDDY_MEMORY_ALLOCATION_H
#define BUDDY_MEMORY_ALLOCATION_H

#include <stdio.h>
#include <stdlib.h>

/* total size: 2^LEVEL_SIZE */
#define LEVEL_SIZE  (10)

/* type definitions */
typedef unsigned char		uint8_t;
typedef unsigned short		uint16_t;
typedef unsigned int		uint32_t;

typedef void *(*alloc_func_t)(uint32_t);
typedef void (*free_func_t)(void *);

typedef struct {
	uint32_t	offset;
	uint16_t	list_idx;
	uint16_t	node_idx;
} ret_t;


/* function definitions */
void *buddy_init(alloc_func_t alloc_hook, free_func_t free_hook);
void *buddy_alloc(uint32_t size, ret_t *);
void buddy_free(ret_t *);
void buddy_destroy();
void buddy_dump();

#endif
